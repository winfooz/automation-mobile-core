package com.winfooz.mobile.drivers;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public abstract class BaseDriver<D extends AppiumDriver> {

    private final ThreadLocal<AppiumDriver> driverThreadLocal = new ThreadLocal<>();

    public abstract AppiumDriver createDriver();

    public AppiumDriver getDriver() {
        return driverThreadLocal.get();
    }

    public abstract DesiredCapabilities getDesiredCapabilities();

    public void setup() {
        driverThreadLocal.set(createDriver());
    }
}
