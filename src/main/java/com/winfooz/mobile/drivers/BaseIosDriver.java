package com.winfooz.mobile.drivers;

import com.winfooz.mobile.common.Config;
import com.winfooz.mobile.models.DeviceWrapper;
import io.appium.java_client.ios.IOSDriver;
import lombok.SneakyThrows;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BaseIosDriver extends BaseDriver<IOSDriver> {

    @SneakyThrows
    public IOSDriver createDriver() {
        return new IOSDriver(Config.getInstance().getBaseUrl(), getDesiredCapabilities());
    }

    @Override
    public DesiredCapabilities getDesiredCapabilities() {
        DeviceWrapper deviceWrapper = DeviceWrapper.create();
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformVersion", deviceWrapper.getIos().getPlatformName());
        desiredCapabilities.setCapability("deviceName", deviceWrapper.getIos().getDeviceName());
        desiredCapabilities.setCapability("deviceName", deviceWrapper.getIos().getUdid());
        desiredCapabilities.setCapability("deviceName", deviceWrapper.getIos().getAutomationName());
        return desiredCapabilities;
    }
}
