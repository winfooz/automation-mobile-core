package com.winfooz.mobile.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public class Config {

    private static final String BASE_URL = "baseUrl";
    private static final String PLATFORM_NAME = "platformName";

    private static Config instance;

    private final Properties properties;

    private Config() {
        File file = new File("src/main/resources/config/config.properties");
        properties = new Properties();
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            properties.load(bufferedReader);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Config getInstance() {
        if (instance == null) {
            synchronized (Config.class) {
                if (instance == null) {
                    instance = new Config();
                }
            }
        }
        return instance;
    }

    public URL getBaseUrl() {
        try {
            return new URL(properties.getProperty(BASE_URL));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public String getPlatformName() {
        return properties.getProperty(PLATFORM_NAME);
    }
}
