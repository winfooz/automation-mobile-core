package com.winfooz.mobile.base;

import com.winfooz.mobile.common.Config;
import com.winfooz.mobile.drivers.BaseAndroidDriver;
import com.winfooz.mobile.drivers.BaseDriver;
import com.winfooz.mobile.drivers.BaseIosDriver;
import io.appium.java_client.AppiumDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.HashMap;
import java.util.Map;

public class BaseTest {

    private final Map<String, BaseDriver<?>> drivers = new HashMap<>();

    public BaseTest() {
        drivers.put("android", new BaseAndroidDriver());
        drivers.put("ios", new BaseIosDriver());
    }

    @BeforeMethod
    public void setupClass() {
        getBaseDriver().setup();
    }

    private BaseDriver getBaseDriver() {
        return drivers.get(Config.getInstance().getPlatformName());
    }

    @AfterMethod
    public void tearDown() {
        AppiumDriver driver = getDriver();
        if (driver != null) {
            driver.quit();
        }
    }

    public AppiumDriver getDriver() {
        return getBaseDriver().getDriver();
    }

}
